/* LICENSE START */
/*
 * ssoup
 * https://github.com/alebellu/ssoup
 *
 * Copyright 2012 Alessandro Bellucci
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * http://alebellu.github.com/licenses/GPL-LICENSE.txt
 * http://alebellu.github.com/licenses/MIT-LICENSE.txt
 */
/* LICENSE END */

/* HEADER START */
var env, requirejs, define;
if (typeof module !== 'undefined' && module.exports) {
    env = "node";
    requirejs = require('requirejs');
    define = requirejs.define;

    requirejs.config({
        baseUrl: __dirname,
        nodeRequire: require
    });

    // require all dependant libraries, so they will be available for the client to request.
    var pjson = require('./package.json');
    for (var library in pjson.dependencies) {
        require(library);
    }
} else {
    env = "browser";
    //requirejs = require;
}

({ define: env === "browser"
        ? define
        : function(A,F) { // nodejs.
            var path = require('path');
            var moduleName = path.basename(__filename, '.js');
            console.log("Loading module " + moduleName);
            module.exports = F.apply(null, A.map(require));
            global.registerUrlContext("/" + moduleName + ".js", {
                "static": __dirname,
                defaultPage: moduleName + ".js"
            });
            global.registerUrlContext("/" + moduleName, {
                "static": __dirname
            });
        }
}).
/* HEADER END */
define(['jquery', 'mongodb', 'artusi-kitchen-tools'], function($, mongodb, tools) {

    var drdf = tools.drdf;
    var drff = tools.drff;

    var mongoDrawerType = function(context) {
        var drawer = this;
        drawer.context = context;
    };

    /**
     * @options:
     *   @drawerIRI the IRI of the drawer.
     */
    mongoDrawerType.prototype.init = function() {
        var dr = $.Deferred();
        var drawer = this;
        var kitchen = drawer.context.kitchen;

        // copy mongodb specific options into easy typing options.
        drawer.conf.mongoAuthIRI = drawer.conf["amd:authIRI"];

        /**
         * Registry indexes.
         */
        drawer.indexes = {};

        /**
         * Code of the recipes.
         */
        drawer.recipesCode = {};

        drawer.recipeBookCache = {};

        drawer.db = kitchen.shelf.authInfo[drawer.conf.mongoAuthIRI]["specificAuthInfo"].db;

        drawer.db.collection("context", function(err, collection) {
            if(err) { dr.reject(err); return; }

            collection.findOne({}, function(err, contextDocument) {
                if(err) { dr.reject(err); return; }

                drawer.context = contextDocument["@context"];

                dr.resolve();
            });
        });

        return dr.promise();
    };

    mongoDrawerType.prototype.getIcon32Url = function() {
        return "http://www.mongodb.org/download/attachments/132305/PoweredMongoDBbeige50.png?version=1&modificationDate=1247081511792";
    };

    mongoDrawerType.prototype.getServiceRecipes = function(options) {
        var drawer = this;
        var dr = $.Deferred();

        dr.resolve([]);

        return dr.promise();
    };

    mongoDrawerType.prototype.ssoupRecipe = function(recipeIRI, recipeCode) {
        this.recipesCode[recipeIRI] = recipeCode;
    };

    /**
     * Artusi implementation of SSOUP [drawer.knows](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.knows)
     *
     * @options:
     * 	@resourceIRI the IRI of the resource to check the existence
     * @return true if the registry contains the resource with the given IRI as a subject of at least one triple, false otherwise.
     */
    mongoDrawerType.prototype.knows = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        drawer.getResource(options).done(function(resource) {
            if (resource) {
                dr.resolve(true);
            } else {
                dr.resolve(false);
            }
        }).fail(drff(dr));

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.get](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.get)
     *
     * @options:
     *   @resourceIRI the IRI of the resource to retrieve
     *   @postProcess how to post process the resource: none, eval, jQuery; defaults to eval
     * @return the resource.
     */
    mongoDrawerType.prototype.getResource = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        var resourceIRI = drawer.applyContext(options.resourceIRI);
        var namespace = tools.getNamespace(resourceIRI);
        if (!namespace) {
            namespace = "ssoup";
        }

        console.info("Retrieving document " + resourceIRI + " in collection " + namespace);
        drawer.db.collection(namespace, function(err, collection) {
            if(err) { console.log("Could not open collection " + namespace); dr.reject(err); return; }

            collection.findOne({"@id": resourceIRI}, function(err, resource) {
                if(err) { console.log("Could not retrieve document " + resourceIRI + " in collection " + namespace); dr.reject(err); return; }

                console.log("Document " + resourceIRI + " in collection " + namespace + " retrieved: " + resource);
                dr.resolve(resource);
            });
        });

        return dr.promise();
    };

    /**
     * Artusi implementation of SSOUP [drawer.put](https://github.com/alebellu/ssoup/blob/master/concepts.md#drawer.put)
     *
     * @options:
     *    @resourceIRI the IRI of the resource to save
     *    @data the data to save to the register
     */
    mongoDrawerType.prototype.put = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        // resourceIRI can be either absolute or relative to the drawer path
        var resourceIRI = drawer.getRelativeResourceIRI(options.resourceIRI);
        //var path = drawer.conf.path + '/' + resourceIRI;

        resourceIRI = drawer.applyContext(options.resourceIRI);
        var namespace = tools.getNamespace(resourceIRI);
        if (!namespace) {
            namespace = "ssoup";
        }

        drawer.db.collection(namespace, function(err, collection) {
            if(err) { dr.reject(err); return; }

            console.info("Updating collection " + namespace + ", document @id=" + resourceIRI);
            collection.update({"@id": resourceIRI}, options.data, {w: 1, upsert: true}, function(err, nrows) {
                console.info("Collection " + namespace + ", document @id=" + resourceIRI + " updated. (" + nrows + " rows).");
                dr.resolve();
            });
        });

        return dr.promise();
    };

    /**
     * Retrieves info about the indexes in the drawer.
     *
     * @options
     */
    mongoDrawerType.prototype.getIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    /**
     * Build all the indexes by scanning all the resources in the drawer.
     *
     * @options
     */
    mongoDrawerType.prototype.buildIndexes = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    }

    /**
     * Build an index by scanning all the resources in the drawer.
     *
     * @options
     *	@indexInfo the info of the index to rebuild.
     */
    mongoDrawerType.prototype.buildIndex = function(options) {
        var dr = $.Deferred();
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    /**
     * Update an index by rescanning all the resources in the specified tree.
     * Blob fetching can be parallelized.
     *
     * @options
     *	@indexInfo the info structure about the index to build
     *	@index the index to update.
     *	@tree the tree to scan.
     * @return an indexing structure for the tree
     */
    mongoDrawerType.prototype.buildIndexForTree = function(options) {
        var deferreds = [];
        var drawer = this;

        dr.resolve();

        return dr.promise();
    };

    return mongoDrawerType;
});
